import { locationCoords } from './../model/model';
import { environment } from './../../environments/environment';
// import { locationCoords } from 'src/app/model/model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { locationCoords } from '../model/model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  APIurl = 'http://localhost:3000/saveCoords';
  coordUrl = 'http://localhost:3000/getCoords';
  distanceUrl = 'http://localhost:3000/distance';

  // APIurl = 'https://backtoneobloodhero.herokuapp.com/saveCoords';
  // coordUrl = 'https://backtoneobloodhero.herokuapp.com/getCoords';
  // distanceUrl = 'https://backtoneobloodhero.herokuapp.com/distance';

  data: locationCoords;
  latitude: number;
  longitude: number;

  constructor(private http: HttpClient) { }

   setCoords(lat: number, long: number) {

    console.log("Masuk setCoords");

    this.latitude = lat;
    console.log("latitude => ", this.latitude);
    this.longitude = long;
    console.log("longitude => ", this.longitude);

    this.data = {
      latitude: lat,
      longitude: long
    }
    // return this.updateCoords(this.data);

    const reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': environment.jwt_token
    });

  console.log("Masukkin ke database");
  return this.http.post(this.APIurl, this.data, {headers: reqHeader});

  }

  // updateCoords(coordinates: locationCoords){

  //   console.log("UpdateCoords latitude => ", coordinates.latitude);
  //   console.log("UpdateCoords longitude => ", coordinates.longitude);

  //   const reqHeader = new HttpHeaders({
  //       'Content-Type': 'application/json',
  //       'Authorization': environment.jwt_token
  //     });

  //   console.log("Masukkin ke database");
  //   this.http.post(this.APIurl, coordinates, {headers: reqHeader});

  //   // return hasil;
  // }

  getCoordUser() {
    const reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': environment.jwt_token
    });

    return this.http.get(this.coordUrl, {headers: reqHeader});
  }

  countNearbyPeople(latitude: number, longitude: number, bloodtype: string, rhesus: string) {
    const reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': environment.jwt_token
    });

    return this.http.post(this.distanceUrl, {latitude, longitude, bloodtype, rhesus}, {headers: reqHeader});
  }


}
