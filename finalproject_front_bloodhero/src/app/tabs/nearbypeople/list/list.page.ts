import { environment } from './../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ListUser } from './../../../model/model';
import { DataService } from './../../data.service';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { locationCoords } from 'src/app/model/model';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {


  notifUrl = 'http://localhost:3000/notifications';

  constructor(
    private alertController: AlertController,
    private router: Router,
    private dataSvc: DataService,
    private http: HttpClient
  ) { }

    // userBloodType = jwt_decode(environment.jwt_token).id[0].blood_type;
    // userRhesus = jwt_decode(environment.jwt_token).id[0].rhesus;

    userBloodType: string;
    userRhesus: string;
    tempRhesus :string;

  coba: any;
  dataUser: ListUser;
  ngOnInit() {
    this.coba = localStorage.getItem('userToken');
    console.log("coba => ", this.coba);
    this.dataUser = JSON.parse(this.coba);
    console.log("dataUser => ", this.dataUser);

    
    
    this.userBloodType = localStorage.getItem('searchBlood');
    console.log('Darah yang dicari =>', this.userBloodType);
    // this.tempRhesus = localStorage.getItem('searchRhesus');
    // console.log('temp rhesus => ', this.tempRhesus);

    // if(this.tempRhesus === "Rh_positive") {
    //   this.userRhesus = "RH+"
    // } else if(this.tempRhesus === "Rh_negative") {
    //   this.userRhesus = "RH-"
    // }
    this.userRhesus = localStorage.getItem('searchRhesus');
    console.log('Rhesus yang dicari =>', this.userRhesus);

    

    for(var i = 0; i<this.dataUser.data.length; i++) {
      this.addNotifications(this.dataUser.data[i].userData.user_id, this.dataUser.data[i].distance).subscribe((response:any) => {
        if(response.code == 200) console.log("response addNotif => ", response);
      })
    }
    // console.log("data user => ", this.dataUser.data[0].userData.name);
    // console.log("distance ke user tersebut => ", this.dataUser.data[0].distance + " KM");
    // this.coords = this.dataSvc.getCoords();
  }

  addNotifications(id:number, jarak:number){
    let data = {
      userId: id,
      distance: jarak
    }

    const reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': environment.jwt_token
    });

    return this.http.post(this.notifUrl, data, {headers: reqHeader});
  }

  // ionViewWillEnter(){
  //   this.getUsers();
  // }
  async endConfirmationAlert() {
    const alert = await this.alertController.create({
      header: 'This list will be deleted',
      message: '<strong>If you continue, this list will be erased and you will be returned to Search Page.<br>Are you sure to continue?</strong>',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on endConfirmationAlert');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.router.navigateByUrl('/home/tabs/nearbypeople');
            console.log('Confirm yes on endConfirmationAlert');
          }
        }
      ]
    });

    await alert.present();
  }
  // /home/tabs/nearbypeople/found
  detailPage(id:number){
    this.router.navigate(['/', 'home', 'tabs', 'nearbypeople', 'found', id]);
  }

}
