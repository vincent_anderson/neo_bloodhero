import { HttpErrorResponse } from '@angular/common/http';
import { DataService } from './../../data.service';
import { Router } from '@angular/router';
// import { DataService } from './../../data.service';
import { environment } from 'src/environments/environment';
// import { DataService } from './../../../nearbypeople/data.service';
import { Geolocation } from '@capacitor/core';
import { locationCoords } from './../../../model/model';
import { Component, OnInit } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
// import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ModalController, AlertController, NavController, ToastController} from '@ionic/angular';
import { single } from 'rxjs-compat/operator/single';
import { parse } from 'querystring';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  // locationCoords: any;
  // timetest: any;

  // coords: locationCoords;

  bloodType: string;
  // rhesus:string;
  latitude: number;
  longitude: number;

  cek_configure: boolean = false;
  dataSendiri: boolean = false;
  // bloodType: string;
  rhesus: string;

  constructor(
    // private androidPermissions: AndroidPermissions,
    // private geolocation: Geolocation,
    // private locationAccuracy: LocationAccuracy,
    private alertController: AlertController, 
    private router: Router, 
    private dataSvc: DataService, 
    public navCtrl: NavController,
    private toastController: ToastController
  ) { }


  ngOnInit() {
    // this.coords.latitude = 0;
    // this.coords.longitude = 0;
    //  this.locationCoords = {
    //    latitude: "",
    //    longitude: "",
    //    accuracy: "",
    //    timestamp: ""
    //  }
    //  this.timetest = Date.now();
  }

  getLoc()
  {
    this.getLocation();
  }



 async getLocation(){
   if(this.cek_configure == true){
   console.log("masuk getLocation");
   const position = await Geolocation.getCurrentPosition();

   this.latitude = position.coords.latitude;
   this.longitude = position.coords.longitude;


    // parseFloat(this.latitude)
    // parseFloat(this.longitude)
    this.dataSvc.setCoords(this.latitude, this.longitude).subscribe((data:any) => {
      if(data.code == 201) { 
        console.log("data: ", data);
        
        this.dataSvc.countNearbyPeople(this.latitude, this.longitude, this.bloodType, this.rhesus).subscribe((data2:any) => {
          if(data2.code == 200) {
            console.log("blood type & rhesus yang dipilih => ", this.bloodType + " " + this.rhesus);
            console.log("data 2 => ", data2);
            localStorage.setItem('userToken', JSON.stringify(data2));
            localStorage.setItem('searchBlood', JSON.stringify(this.bloodType));
            localStorage.setItem('searchRhesus', JSON.stringify(this.rhesus));
            this.router.navigateByUrl('/home/tabs/nearbypeople/list');
          } else if (data2.code == 404) {
            this.noResultToast();
          }
        });
      // this.dataSvc.getCoordUser().subscribe((dataCoords: any) => {
      //   console.log("dataCoords: ", dataCoords);
      // })
    }
    
    },(err: HttpErrorResponse) => {
    });
  }
  //  console.log("list data user => ", dataCoords)
 }

//  getUsers() {
//   this.dataSvc.getCoordUser().subscribe((dataCoords: any) => {
//     console.log("list data dari user => ", dataCoords.data);
//     // console.log("latitude dari user1 ", dataCoords.data[0].latitude);
//   })
// }

  async typeFirstChoice()
  {
    const alert1 = await this.alertController.create({
      header: 'What type of blood you are looking for?',
      inputs: [
        {
          name: 'same',
          type: 'radio',
          label: 'The same with my profile',
          value: 'same'

        },
        {
          name: 'different',
          type: 'radio',
          label: 'Different from my profile',
          value: 'different'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on typeFirstChoice');
          }
        },
        {
          text: 'Done',
          handler: (data: string) => {
            if(data === 'different')
            {
              this.typeSecondChoice();
            } else {
              this.dataSendiri = true;
              this.bloodType = jwt_decode(environment.jwt_token).id[0].blood_type;
              this.rhesus = jwt_decode(environment.jwt_token).id[0].rhesus;
              console.log("bloodType dari user sendiri => ", this.bloodType);
              console.log("Data sendiri status => ", this.dataSendiri);
              this.cek_configure = true;
          }
          }
        }
      ]
    })


    await alert1.present();
  }

  async typeSecondChoice()
  {
    const alert2 = await this.alertController.create({
      header: 'What blood type you are looking for?',
      inputs: [
        {
          name: 'type_A',
          type: 'radio',
          label: 'A',
          value: 'A'
        },
        {
          name: 'type_B',
          type: 'radio',
          label: 'B',
          value: 'B'
        },
        {
          name: 'type_O',
          type: 'radio',
          label: 'O',
          value: 'O'
        },
        {
          name: 'type_AB',
          type: 'radio',
          label: 'AB',
          value: 'AB'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on typeSecondChoice');
          }
        },
        {
          text: 'Done',
          handler: data => {
            console.log("data => ",data);
            this.bloodType = data;
            console.log("bloodType => ", this.bloodType);
            console.log('Confirm done on typeSecondChoice');
            this.typeThirdChoice();
            this.cek_configure = true;
          }
        }
      ]
    })

    await alert2.present();
  }

  async typeThirdChoice()
  {
    const alert3 = await this.alertController.create({
      header: "What rhesus you are looking for?",
      inputs: [
        {
          name: 'Rh_positive',
          type: 'radio',
          label: 'RH+',
          value: 'Rh_positive'
        },
        {
          name: 'Rh_negative',
          type: 'radio',
          label: 'RH-',
          value: 'Rh_negative'
        }
      ],
      buttons:
      [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on typeThirdChoice');
          }
        },
        {
          text: 'Done',
          handler: data => {
            console.log("data => ", data);
            this.rhesus = data;
            console.log("Rhesus=> ", this.rhesus);
            console.log("Confirm done on typeThirdChoice");
          }
        }
      ]
    })

    await alert3.present();
  }

  async noResultToast()
  {
    const toast = await this.toastController.create({
      message: "Sorry, there is no people with this type nearby",
      duration: 2000
    });

    await toast.present();
  }

  // Check application's GPS permission
  // checkGPSPermission()
  // {
  //   this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
  //     result => {
  //       if(result.hasPermission)
  //       {
  //         this.askToTurnOnGPS();
  //       }
  //       else
  //       {
  //         this.requestGPSPermission();
  //       }
  //     },
  //     err => {
  //       alert(err);
  //     }
  //   );
  // }

  // requestGPSPermission()
  // {
  //   this.locationAccuracy.canRequest().then((canRequest : boolean) => {
  //     if(canRequest)
  //     {
  //       console.log("4");
  //     }
  //     else
  //     {
  //       //show 'GPS Permission Request' dialogue
  //       this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
  //       .then(
  //         () => {
  //           this.askToTurnOnGPS();
  //         },
  //         error => {
  //           alert('requestPermission Error requestion location permission')
  //         }
  //       );
  //     }
  //   });
  // }

  // askToTurnOnGPS()
  // {
  //   this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
  //     () => {
  //       this.getLocationCoordinates()
  //     },
  //     error => alert('Error requesting location permissions')
  //   );
  // }

  // getLocationCoordinates()
  // {
  //   this.geolocation.getCurrentPosition().then((resp) => {
  //     this.locationCoords.latitude = resp.coords.latitude;
  //     this.locationCoords.longitude = resp.coords.longitude;
  //     this.locationCoords.accuracy = resp.coords.accuracy;
  //     this.locationCoords.timestamp = resp.timestamp;
  //   }).catch((error) => {
  //     alert('Error getting location')
  //   });
  // }

}
