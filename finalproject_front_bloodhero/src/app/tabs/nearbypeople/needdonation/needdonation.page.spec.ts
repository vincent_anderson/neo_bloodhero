import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeeddonationPage } from './needdonation.page';

describe('NeeddonationPage', () => {
  let component: NeeddonationPage;
  let fixture: ComponentFixture<NeeddonationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeeddonationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeeddonationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
