import { Router } from '@angular/router';
import { Notifications } from './../../../model/model';
import { environment } from './../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ScrollDetail } from '@ionic/core';

@Component({
  selector: 'app-needdonation',
  templateUrl: './needdonation.page.html',
  styleUrls: ['./needdonation.page.scss'],
})
export class NeeddonationPage implements OnInit {

  showToolbar = false;

  getNotifUrl = 'http://localhost:3000/getnotifications';

  notif: Notifications;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit() {
    this.getNotifications().subscribe((data:any) => {
      if (data.code === 200) {
        // console.log("data => ",data);
        this.notif = data.data;
        console.log("Notif => ", this.notif);
      } else if(data.code === 404) {

      }
    }
    )
  }

  // /home/tabs/nearbypeople/recipient
  needProfile(id:number) {
    this.router.navigate(['/', 'home', 'tabs', 'nearbypeople', 'recipient', id]);
  }
  ionViewWillEnter(){
    // this.getNotifications().subscribe((data:any) =>{
    //   if(data.code === 200) {
    //     // console.log("data => ",data);
    //     this.notif = data.data;
    //     console.log("Notif => ", this.notif);
    //   }
    // }
    // )
  }

  getNotifications(){
    const reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': environment.jwt_token
    });

    return this.http.get(this.getNotifUrl, {headers: reqHeader});
  }

  onScroll($event: CustomEvent<ScrollDetail>)
  {
    if ($event && $event.detail && $event.detail.scrollTop)
    {
      const scrollTop = $event.detail.scrollTop;
      this.showToolbar = scrollTop >= 225;
    }
  }

}
