import { environment } from './../../../../environments/environment';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { DetailUser } from './../../../model/model';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-foundneedprofile',
  templateUrl: './foundneedprofile.page.html',
  styleUrls: ['./foundneedprofile.page.scss'],
})
export class FoundneedprofilePage implements OnInit {

  detailUrl = 'http://localhost:3000/founduserprofile';

  constructor(
    public actionSheetController : ActionSheetController,
    private urlSvc: ActivatedRoute,
    private http: HttpClient
    ) { }

    paramId: number;
    dataUser: DetailUser;

  ngOnInit() {
    this.urlSvc.paramMap.subscribe( url => {
        this.paramId = + url.get('userId');
        console.log("paramID => ",this.paramId);

        this.getFoundUser(this.paramId).subscribe((detailUser:any) => {
          // console.log("detail user => ", detailUser);
          if(detailUser.code === 200) {
            console.log("detail user 2 => ", detailUser);
            this.dataUser = detailUser.data[0];
            console.log("data user => ", this.dataUser.name);
          }
        })
    }, (err: HttpErrorResponse) => {
    })
  }

  getFoundUser(id:number){
    let data = {
      userId : id
    }
    const reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': environment.jwt_token
    });
      return this.http.post(this.detailUrl, data, {headers: reqHeader})
}


  async callActionSheet()
  {
    const actionSheet = await this.actionSheetController.create({
      header: 'Call with...',
      buttons: [{
        text: 'Call with Phone',
        icon: 'call',
        handler: () =>
        {
          console.log('Call with Phone clicked');
        }
      },
      {
        text: 'Contact via Whatsapp',
        icon: 'logo-whatsapp',
        handler: () => 
        {
          console.log('Contact via Whatsapp clicked');
        }
      },
      {
        text: 'Close',
        icon: 'close',
        role: 'cancel',
        handler: () =>
        {
          console.log('Close clicked');
        }
      }
    ]
    });
    
    await actionSheet.present();
  }

}
