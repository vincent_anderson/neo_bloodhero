import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FoundneedprofilePage } from './foundneedprofile.page';

const routes: Routes = [
  {
    path: '',
    component: FoundneedprofilePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FoundneedprofilePage]
})
export class FoundneedprofilePageModule {}
