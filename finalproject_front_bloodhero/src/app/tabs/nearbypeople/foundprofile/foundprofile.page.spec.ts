import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoundprofilePage } from './foundprofile.page';

describe('FoundprofilePage', () => {
  let component: FoundprofilePage;
  let fixture: ComponentFixture<FoundprofilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoundprofilePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundprofilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
