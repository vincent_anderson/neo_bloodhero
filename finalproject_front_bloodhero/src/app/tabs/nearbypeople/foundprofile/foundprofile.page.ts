import { DetailUser } from './../../../model/model';
import { environment } from './../../../../environments/environment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-foundprofile',
  templateUrl: './foundprofile.page.html',
  styleUrls: ['./foundprofile.page.scss'],
})
export class FoundprofilePage implements OnInit {

  detailUrl = 'http://localhost:3000/founduserprofile';

  constructor(
    private router: Router,
    public actionSheetController: ActionSheetController, 
    private callNumber: CallNumber,
    private urlSvc: ActivatedRoute,
    private http: HttpClient
    ) { }

    paramId: number;
    dataUser: DetailUser;

  ngOnInit() {
    this.urlSvc.paramMap.subscribe( url => {
      this.paramId = + url.get('userId');
      console.log(this.paramId);

      this.getFoundUser(this.paramId).subscribe((detailUser:any) => {
        console.log("detail user => ", detailUser);
        if(detailUser.code === 200) {
          console.log("detail user 2 => ", detailUser);
          this.dataUser = detailUser.data[0];
          console.log("data user => ", this.dataUser.name);
        }
      })

    }, (err: HttpErrorResponse) => {
    })
  }

  getFoundUser(id:number){

    let data = {
      userId : id
    }
    const reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': environment.jwt_token
    });
      return this.http.post(this.detailUrl, data, {headers: reqHeader})
  }

  async callActionSheet()
  {
    const actionSheet = await this.actionSheetController.create({
      header: 'Call with...',
      buttons: [{
        text: 'Call with Phone',
        icon: 'call',
        handler: () =>
        {
          console.log('Call with Phone clicked');
          this.callNow(this.dataUser.phone_number); // ('') <- phone number
        }
      },
      {
        text: 'Contact via Whatsapp',
        icon: 'logo-whatsapp',
        handler: () => 
        {
          console.log('Contact via Whatsapp clicked');
        }
      },
      {
        text: 'Close',
        icon: 'close',
        role: 'cancel',
        handler: () =>
        {
          console.log('Close clicked');
        }
      }
    ]
    });

    await actionSheet.present();
  }

  callNow(number)
  {
    this.callNumber.callNumber(number, true)
      .then(res => console.log('Launcing Dialler', res))
      .catch(err => console.log('Error launching Dialler', err))
  }


}