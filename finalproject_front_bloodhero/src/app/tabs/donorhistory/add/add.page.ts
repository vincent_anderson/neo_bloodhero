import { Component, OnInit } from '@angular/core';
import { HistoryService } from '../history.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { history } from 'src/app/model/model';
// import { HistoryPage} from 'src/app/tabs/donorhistory/history/history.page';
@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {

  form : FormGroup;
  saved_date : string;
  saved_desc : string;
  number_of_data : number;
  histories: history[];
  constructor(
    private historySvc : HistoryService,
    private formBuilder : FormBuilder,
    private alertController : AlertController,
    private router: Router,
    // private historyPage : HistoryPage
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      history_date:
      [null, 
       Validators.required],
      history_desc:
      [null,
       Validators.compose(
         [Validators.required,
          Validators.maxLength(250)]
       )]
    });
    console.log(this.form);
  }

  // IonViewDidLeave()
  // {
  //   this.router.navigateByUrl('/home/tabs/donorhistory');
  //   this.historySvc.returnHistory().subscribe((data: any) => {
  //     this.histories = data.data;
  //     this.number_of_data = data.data.length;
  //     console.log("Saved: ", this.histories);
  //   });
  // }

  saveHistory()
  {
    this.saved_date = this.form.value.history_date;
    this.saved_date = this.saved_date.slice(0, 10);
    console.log("Saved Data: ", this.saved_date);
    this.saved_desc = this.form.value.history_desc;
    console.log("Saved Desc: ", this.saved_desc);

    this.historySvc.addHistory(this.saved_date, this.saved_desc).subscribe((data: any) => {
      console.log("Data: ", data);
      if(data.code == 500)
      {
        console.log("Server down");
        this.serverError();
      }
      else
      {
        console.log("Insert done");
        this.haveFinishedConfirmation();
      }
    })
  }

  async haveFinishedConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Success',
      message: 'Your donor story has been saved',
      buttons:
      [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log("Confirm close on haveFinishedConfirmation");
            // this.historyPage.reloadData();
            this.router.navigateByUrl('/home/tabs/donorhistory');
            this.historySvc.returnMyHistory();
          }
        }
      ]
    });

    await alert.present();
  }

  async serverError()
  {
    const error_alert = await this.alertController.create({
      header: 'Error',
      message: 'Connection to server can\'t be established for now. Check your internet connection',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Confirm close on serverError');
            this.router.navigateByUrl('/home/tabs/donorhistory');
          }
        }
      ]
    });

    await error_alert.present();
  }



}
