import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { history } from 'src/app/model/model';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  // ENDPOINT
  url_add_history = 'https://backtoneobloodhero.herokuapp.com/savehistory';
  url_show_history = 'https://backtoneobloodhero.herokuapp.com/showhistory';
  histories: history[];
  number_of_data: number;

  constructor(private http: HttpClient) { }

  returnHistory()
  {
    console.log("Enter returnHistory");
    const reqHeader = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': environment.jwt_token
      })
    };
    console.log("reqHeader: ", reqHeader);

    return this.http.get(this.url_show_history, reqHeader);
  }

  addHistory(date: string, desc: string)
  {
    console.log("Enter addHistory");
    const reqHeader = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': environment.jwt_token
      })
    };
    console.log("reqHeader: ", reqHeader);

    return this.http.post(this.url_add_history, {date, desc}, reqHeader);
  }

  returnMyHistory()
  {
    this.returnHistory().subscribe((data: any) => {
      if(data.code == 200)
      {
        this.histories = data.data;
        this.number_of_data = data.data.length;
        console.log("Saved: ", this.histories);
      }
    })
  }
}
