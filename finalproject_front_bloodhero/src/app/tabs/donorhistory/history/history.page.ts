import { Component, OnInit } from '@angular/core';
import { ScrollDetail } from '@ionic/core';
import { HistoryService } from '../history.service';
import { history } from 'src/app/model/model';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {

  showToolbar = false;
  histories : history[];
  number_of_data : number;
  modify_date : string;
  itr : number;

  constructor(
    private historySvc : HistoryService
  ) { }

  ngOnInit() {
     this.historySvc.returnHistory().subscribe((data: any) => {
      if(data.code == 200)
      {
        this.histories = data.data;
        this.number_of_data = data.data.length;
        console.log("Saved: ", this.histories);
      }
    });
  }
  
  IonViewDidEnter()
  {
    // this.historySvc.returnHistory().subscribe((data: any) => {
    //   this.histories = data.data;
    //   this.number_of_data = data.data.length;
    //   console.log("Saved: ", this.histories);
    // });
    this.historySvc.returnMyHistory();
  }

  IonViewWillEnter()
  {
    // this.historySvc.returnHistory().subscribe((data: any) => {
    //   this.histories = data.data;
    //   this.number_of_data = data.data.length;
    //   console.log("Saved: ", this.histories);
    // });
    this.historySvc.returnMyHistory();
  }



  onScroll($event: CustomEvent<ScrollDetail>)
  {
    if($event && $event.detail && $event.detail.scrollTop)
    {
      const scrollTop = $event.detail.scrollTop;
      this.showToolbar = scrollTop >= 225;
    }
  }

  doRefresh($event)
  {
    console.log("Begin Refreshing");
    setTimeout(() => {
      console.log("Doing operation");
      this.historySvc.returnMyHistory();
      $event.target.complete();
    }, 2000);
  }
}
