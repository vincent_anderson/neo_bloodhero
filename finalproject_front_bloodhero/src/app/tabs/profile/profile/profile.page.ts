import { LoginService } from './../../../start/login/login.service';
import { Component, OnInit } from '@angular/core';
import { AlertController, ActionSheetController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { File } from '@ionic-native/file/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ProfileService } from './profile.service';
import { HttpErrorResponse } from '@angular/common/http';
import { updateProfileData } from 'src/app/model/model';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  // updateData: updateProfileData;
  form: FormGroup;
  isLoading = false;
  imagePickerOptions = 
  {
    maximumImagesCount: 1,
    quality: 50
  }
  
  display_name : string;
  display_email : string;
  display_address : string;
  display_phone_number : string;
  display_profile_picture : string;
  display_blood_type : string;
  display_rhesus : string;

  tempImage: SafeResourceUrl;
  profPic : any;

  name : string;
  address : string;
  phone_number : string;
  profile_picture : string;

  profilePicture: string = "consider_it_exists";

  constructor(
    private alertController: AlertController, 
    private router : Router, 
    private loginService: LoginService, 
    private formBuilder: FormBuilder,
    private file: File,
    private camera: Camera,
    private actionSheetCtrl: ActionSheetController,
    private loadingCtrl: LoadingController,
    private profileSvc : ProfileService,
    private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.profileSvc.returnProfile().subscribe((data: any) => {
      console.log("Loaded Data: ", data);
      // 400, 200
      if(data.code == 400)
      {
        this.notFoundConfirmation();
      }
      else if(data.code == 200)
      {
        console.log("Loaded Data | Name: ", data.data[0].name);
        this.display_name = data.data[0].name;
        console.log("Loaded Data | Email: ", data.data[0].email);
        this.display_email = data.data[0].email;
        console.log("Loaded Data | Address: ", data.data[0].address);
        this.display_address = data.data[0].address;
        console.log("Loaded Data | Phone Number: ", data.data[0].phone_number);
        this.display_phone_number = data.data[0].phone_number;
        console.log("Loaded Data | Profile Picture: ", data.data[0].profile_picture);
        this.display_profile_picture = data.data[0].profile_picture;
        console.log("Loaded Data | Blood Type: ", data.data[0].blood_type);
        this.display_blood_type = data.data[0].blood_type;
        console.log("Loaded Data | Rhesus: ", data.data[0].rhesus);
        if(data.data[0].rhesus === "Rh_negative")
        {
          this.display_rhesus = "RH-";
        }
        else if(data.data[0].rhesus === "Rh_positive")
        {
          this.display_rhesus = "RH+";
        }
      }
    },
    (err: HttpErrorResponse) => {
      console.log(err);
    });

    this.form = this.formBuilder.group({
      fullName:
        [null, Validators.compose(
          [Validators.required,
          Validators.pattern('[a-zA-Z]*'),
          Validators.maxLength(30)]
        )],
      address:
        [null,
        Validators.required],
      phoneNumber:
        [null,
        Validators.compose(
          [ Validators.pattern('[0-9]*'),
          Validators.minLength(5),
          Validators.required]
        )]
    });

    console.log(this.form);

    //show user's profile picture for avatar slot by default
  }

  IonViewDidEnter()
  {
    console.log("IonViewDidEnter()");
    this.profileSvc.returnProfile().subscribe((data: any) => {
      console.log("Loaded Data: ", data);
      // 400, 200
      if(data.code == 400)
      {
        this.notFoundConfirmation();
      }
      else if(data.code == 200)
      {
        console.log("Loaded Data | Name: ", data.data[0].name);
        this.display_name = data.data[0].name;
        console.log("Loaded Data | Email: ", data.data[0].email);
        this.display_email = data.data[0].email;
        console.log("Loaded Data | Address: ", data.data[0].address);
        this.display_address = data.data[0].address;
        console.log("Loaded Data | Phone Number: ", data.data[0].phone_number);
        this.display_phone_number = data.data[0].phone_number;
        console.log("Loaded Data | Profile Picture: ", data.data[0].profile_picture);
        this.display_profile_picture = data.data[0].profile_picture;
        console.log("Loaded Data | Blood Type: ", data.data[0].blood_type);
        this.display_blood_type = data.data[0].blood_type;
        console.log("Loaded Data | Rhesus: ", data.data[0].rhesus);
        if(data.data[0].rhesus === "Rh_negative")
        {
          this.display_rhesus = "RH-";
        }
        else if(data.data[0].rhesus === "Rh_positive")
        {
          this.display_rhesus = "RH+";
        }
      }
    },
    (err: HttpErrorResponse) => {
      console.log(err);
    });

     this.form = this.formBuilder.group({
      fullName:
        [this.display_name, Validators.compose(
          [Validators.required,
          Validators.pattern('[a-zA-Z]*'),
          Validators.maxLength(30)]
        )],
      address:
        [this.display_address,
        Validators.required],
      phoneNumber:
        [this.phone_number,
        Validators.compose(
          [ Validators.pattern('[0-9]*'),
          Validators.minLength(5)]
        )]
    });
  }

  IonViewWillEnter()
  {
    console.log("IonViewWillEnter()");
    this.profileSvc.returnProfile().subscribe((data: any) => {
      console.log("Loaded Data: ", data);
      // 400, 200
      if(data.code == 400)
      {
        this.notFoundConfirmation();
      }
      else if(data.code == 200)
      {
        console.log("Loaded Data | Name: ", data.data[0].name);
        this.display_name = data.data[0].name;
        console.log("Loaded Data | Email: ", data.data[0].email);
        this.display_email = data.data[0].email;
        console.log("Loaded Data | Address: ", data.data[0].address);
        this.display_address = data.data[0].address;
        console.log("Loaded Data | Phone Number: ", data.data[0].phone_number);
        this.display_phone_number = data.data[0].phone_number;
        console.log("Loaded Data | Profile Picture: ", data.data[0].profile_picture);
        this.display_profile_picture = data.data[0].profile_picture;
        console.log("Loaded Data | Blood Type: ", data.data[0].blood_type);
        this.display_blood_type = data.data[0].blood_type;
        console.log("Loaded Data | Rhesus: ", data.data[0].rhesus);
        if(data.data[0].rhesus === "Rh_negative")
        {
          this.display_rhesus = "RH-";
        }
        else if(data.data[0].rhesus === "Rh_positive")
        {
          this.display_rhesus = "RH+";
        }
      }
    },
    (err: HttpErrorResponse) => {
      console.log(err);
    });

    this.form = this.formBuilder.group({
      fullName:
        [this.display_name, Validators.compose(
          [Validators.required,
          Validators.pattern('[a-zA-Z]*'),
          Validators.maxLength(30)]
        )],
      address:
        [this.display_address,
        Validators.required],
      phoneNumber:
        [this.phone_number,
        Validators.compose(
          [ Validators.pattern('[0-9]*'),
          Validators.minLength(5)]
        )]
    });
  }

  onLogout() {
    this.loginService.logout();
    this.router.navigateByUrl('/login');
  }

  changeProfile()
  {
    console.log("Form | Name: ", this.form.value.fullName);
    console.log("Form | Address: ", this.form.value.address);
    console.log("Form | Phone Number: ", this.form.value.phoneNumber);

    this.name = this.form.value.fullName;
    this.address = this.form.value.address;
    this.phone_number = this.form.value.phoneNumber;
    this.profile_picture = this.display_profile_picture;

    this.profileSvc.updateProfile(this.name, this.address, this.phone_number, this.profile_picture).subscribe((data: any) => {
      console.log("Data: ", data);
      if(data.code == 500)
      {
        console.log("Server down");
        this.serverError();
      }
      else if(data.code == 400)
      {
        console.log("Same phone number has been used previously");
        this.phoneNumberAgainError();
      }
      else if(data.code == 201)
      {
        console.log("Update done");
        this.haveFinishedConfirmation();
      }
    },
    (err: HttpErrorResponse) => {
      console.log(err);
    });
  }

  async haveFinishedConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Success',
      message: 'Profile has been updated',
      buttons:
      [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log("Confirm close on haveFinishedConfirmation");
          }
        }
      ]
    });

    await alert.present();
  }
  
  async serverError()
  {
    const error_alert = await this.alertController.create({
      header: 'Error',
      message: 'Connection to server can\'t be established for now. Check your internet connection',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Confirm close on serverError');
          }
        }
      ]
    });

    await error_alert.present();
  }

  async phoneNumberAgainError()
  {
    const alert = await this.alertController.create({
      header: 'Error',
      message: 'This number has been used before. Please enter a different phone number',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Confirm close on phonNumberAgainError');
          }
        }
      ]
    });

    await alert.present();
  }
  

  async notFoundConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Warning',
      message: 'Profile can\'t be found',
      buttons:
      [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log("Confirm close on notFoundConfirmation");
            this.onLogout();
          }
        }
      ]
    });

    await alert.present();
  }
  
  async logoutConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Are you sure to logout?',
      buttons:
      [
        {
          text: 'No',
          handler: () => {
            console.log("Confirm cancel on logoutConfirmation");
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log("Confirm done on logoutConfirmation");
          }
        }
      ]
    });

    await alert.present();
  }

  // pickImage(sourceType)
  // {
  //   const options: CameraOptions = 
  //   {
  //     quality: 100,
  //     sourceType: sourceType,
  //     destinationType: this.camera.DestinationType.FILE_URI,
  //     encodingType: this.camera.EncodingType.JPEG,
  //     mediaType: this.camera.MediaType.PICTURE
  //   }
  //   this.camera.getPicture(options).then((imageData) => {
  //     //if base64 (BASE_URL);
  //     // let base64image = 'data:image/jpeg;base64,' + imageData;
  //     // send image to endpoint /upload here.
  //   }, (err) => {
  //     console.log('Error on pickImage()');
  //   });
  // }

  async showMe(data: any)
  {
     const alert = await this.alertController.create({
       message: data,
       buttons: [{
         text: 'close',
         role: 'cancel'
       }]
     })
  }

  async pickPhoto()
  {
    const { Camera } = Plugins;
    const result = await Camera.getPhoto({
      quality: 100,
      allowEditing: false,
      source: CameraSource.Photos,
      resultType: CameraResultType.DataUrl,
    });

    this.tempImage = this.sanitizer.bypassSecurityTrustResourceUrl(
      result && (result.dataUrl),
    );

    this.profPic = this.tempImage;
    this.showMe(this.profPic);
  }

  async takePhoto()
  {
    const { Camera } = Plugins;
    const result = await Camera.getPhoto({
      quality: 100,
      allowEditing: false,
      source: CameraSource.Camera,
      resultType: CameraResultType.DataUrl,
    });

    this.tempImage = this.sanitizer.bypassSecurityTrustResourceUrl(
      result && (result.dataUrl),
    );

    this.profPic = this.tempImage;
  }

  async selectImage()
  {
    const actionSheetCtrl = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
            console.log("Enter selectImage() on Load from Library");
            this.pickPhoto();
            // this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePhoto();
            // this.pickImage(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });

    await actionSheetCtrl.present();
  }
  
}
