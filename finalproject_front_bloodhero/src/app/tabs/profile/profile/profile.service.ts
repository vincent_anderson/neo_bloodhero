import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { updateProfileData } from 'src/app/model/model';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  // ENDPOINT
  url_active = 'https://backtoneobloodhero.herokuapp.com/active';

  // url_update = 'http://localhost:3000/update';


  url_update = 'https://backtoneobloodhero.herokuapp.com/update';
  url_upload = 'https://backtoneobloodhero.herokuapp.com/upload';

  // url_active = 'http://localhost:3000/active';
  // url_update = 'http://localhost:3000/update';
  // url_upload = 'http://localhost:3000/upload';

  constructor(private http: HttpClient) { }

  returnProfile()
  {
    console.log("Enter returnProfile()");

    const reqHeader = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': environment.jwt_token
      })
    };
    console.log("reqHeader: ", reqHeader);

    return this.http.get(this.url_active, reqHeader);
  }

  upload(profilePicture: File)
  {
    const reqHeader = new HttpHeaders({});

    return this.http.post(this.url_upload, profilePicture, {headers: reqHeader});
  }

  updateProfile(namex: string, addressx: string, phone_numberx: string, profile_picturex: string)
  {
    console.log("Enter updateProfile()");

    const reqHeader = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': environment.jwt_token
      })
    };

    console.log("reqHeader: ", reqHeader);
    return this.http.post(this.url_update, {namex, addressx, phone_numberx, profile_picturex}, reqHeader);
  }
}
