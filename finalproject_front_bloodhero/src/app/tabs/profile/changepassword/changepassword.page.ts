import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ChangepasswordService } from './changepassword.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.page.html',
  styleUrls: ['./changepassword.page.scss'],
})
export class ChangepasswordPage implements OnInit {

  form: FormGroup;
  response_code: number;
  saved_old_password : string;
  saved_new_password : string;

  constructor(
    private changePasswordSvc : ChangepasswordService,
    private alertController: AlertController, 
    private router: Router, 
    private formBuilder : FormBuilder) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      oldPassword:
        [null,
        Validators.compose(
          [Validators.required,
          Validators.minLength(6)]
        )],
      password:
        [null,
        Validators.compose(
          [Validators.required,
          Validators.minLength(6)]
        )],
      rePassword:
        [null,
        Validators.compose(
          [Validators.required,
          Validators.minLength(6)]
        )]
    })
  }

  onChangePassword()
  {
    if(this.form.value.password === this.form.value.rePassword)
    {
      this.saved_old_password = this.form.value.oldPassword;
      this.saved_new_password = this.form.value.password;

      this.changePasswordSvc.changePassword(this.saved_old_password, this.saved_new_password).subscribe((data: any) => {
        this.response_code = data.code;
        console.log("Response code: ", this.response_code);
        if(this.response_code == 500) 
        {
          // server error
          this.serverError();
        }
        else if (this.response_code == 400)
        {
          // password not matched
          this.wrongCurrentPasswordConfirmation();
        }
        else if (this.response_code == 404)
        {
          // user not found
          this.notFoundConfirmation();
        } 
        else if (this.response_code == 200)
        {
          // success
          this.successConfirmation();
        }
      })
    }
    else
    {
      this.unmatchedConfirmation();
      this.form.reset();
      return;
    }
  }

  onDeleteAccount()
  {
    this.changePasswordSvc.deleteAccount().subscribe((data: any) => {
      this.response_code = data.code;
      console.log("Response code: ", this.response_code);
      if(this.response_code == 500)
      {
        this.serverError();
      }
      else if(this.response_code == 200)
      {
        this.successDeleteConfirmation();
      }
    });
  }

  async serverError()
  {
    const alert = await this.alertController.create({
      header: 'Error',
      message: 'Server is currently unreachable',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on serverError');
          }
        }
      ]
    });

    await alert.present();
  }

  async notFoundConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Error',
      message: 'User is not found',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on notFoundConfirmation');
          }
        }
      ]
    });

    await alert.present();
  }

  async successConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Success',
      message: 'Your password has been successfully changed',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on successConfirmation');
          }
        }
      ]
    });

    await alert.present();
  }

  async successDeleteConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Success',
      message: 'Your account has been successfully deleted',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on successDeleteConfirmation()');
          }
        }
      ]
    })
  }

  async unmatchedConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Warning!',
      message: 'Password in New Password and Re-enter New Password are not matched',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on unmatchedConfirmation');
          }
        }
      ]
    });

    await alert.present();
  }

  async wrongCurrentPasswordConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Warning!',
      message: 'Current password is wrong',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on wrongCurrentPasswordConfirmation');
          }
        }
      ]
    });

    await alert.present();
  }

  async deleteAccountConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Are you sure to delete your account permanently?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler:  () => {
            console.log('Confirm cancel on deleteAccountConfirmation');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.onDeleteAccount();
            this.router.navigateByUrl('/login');
            console.log('Confirm done on deleteAccountConfirmation');
          }
        }
      ]
    });

    await alert.present();
  }

  async changeConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Your password will be changed',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on changeConfirmation');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.onChangePassword();
            this.router.navigateByUrl('/home/tabs/profile');
            console.log('Confirm done on changeConfirmation');
          }
        }
      ]
    });

    await alert.present();
  }

}
