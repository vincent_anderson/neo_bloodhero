import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { changePassword } from 'src/app/model/model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChangepasswordService {

  // ENDPOINT
  url_change = 'https://backtoneobloodhero.herokuapp.com/changepassword';
  url_delete = 'https://backtoneobloodhero.herokuapp.com/delete';

  constructor(private http: HttpClient) { }

  changePassword(old_password: string, password: string)
  {
    const reqHeader = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': environment.jwt_token
      })
    };
    console.log("reqHeader: ", reqHeader);

    return this.http.post(this.url_change, {old_password, password}, reqHeader);
  }

  deleteAccount()
  {
    const reqHeader = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': environment.jwt_token
      })
    };

    return this.http.get(this.url_delete, reqHeader);
  }
}
