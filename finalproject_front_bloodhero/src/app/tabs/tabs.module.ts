import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';
import { LoginPageModule } from '../start/login/login.module';
import { Signup1PageModule } from '../start/signup1/signup1.module';
import { Signup2PageModule } from '../start/signup2/signup2.module';
import { SearchPageModule } from './nearbypeople/search/search.module';
import { ListPageModule } from './nearbypeople/list/list.module';
import { FoundneedprofilePageModule } from './nearbypeople/foundneedprofile/foundneedprofile.module';
import { NeeddonationPageModule } from './nearbypeople/needdonation/needdonation.module';
import { FoundprofilePageModule } from './nearbypeople/foundprofile/foundprofile.module';
import { ProfilePageModule } from './profile/profile/profile.module';
import { ChangepasswordPageModule } from './profile/changepassword/changepassword.module';
import { HistoryPageModule } from './donorhistory/history/history.module';
import { AddPageModule } from './donorhistory/add/add.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    LoginPageModule,
    Signup1PageModule,
    Signup2PageModule,
    SearchPageModule,
    ListPageModule,
    ProfilePageModule,
    HistoryPageModule,
    AddPageModule,
    ChangepasswordPageModule,
    FoundneedprofilePageModule,
    NeeddonationPageModule,
    FoundprofilePageModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
