import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'nearbypeople',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tabs/nearbypeople/search/search.module').then(m => m.SearchPageModule)
          },
          {
            path: 'list',
            loadChildren: () =>
              import('../tabs/nearbypeople/list/list.module')
              .then(m => m.ListPageModule)
          },
          {
            path: 'found/:userId',
            loadChildren: () =>
              import('../tabs/nearbypeople/foundprofile/foundprofile.module')
              .then(m => m.FoundprofilePageModule)
          },
          {
            path: 'donate',
            loadChildren: () =>
              import('../tabs/nearbypeople/needdonation/needdonation.module')
              .then(m => m.NeeddonationPageModule)
          },
          {
            path: 'recipient/:userId',
            loadChildren: () =>
              import('../tabs/nearbypeople/foundneedprofile/foundneedprofile.module')
              .then(m => m.FoundneedprofilePageModule)
          }
        ]
      },
      {
        path: 'donorhistory',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tabs/donorhistory/history/history.module')
              .then(m => m.HistoryPageModule)
          },
          {
            path: 'add',
            loadChildren: () =>
              import('../tabs/donorhistory/add/add.module')
              .then(m => m.AddPageModule)
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tabs/profile/profile/profile.module')
              .then(m => m.ProfilePageModule)
          },
          {
            path: 'change',
            loadChildren: () =>
              import('../tabs/profile/changepassword/changepassword.module')
              .then(m => m.ChangepasswordPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/nearbypeople',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/nearbypeople',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
