import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(private alertController: AlertController) { }

  ngOnInit() {
  }

  async bloodTypeAlert() {
    const alert = await this.alertController.create({
      header: 'What is the type of your blood?',
      inputs: [
        {
          name: 'type_A',
          type: 'radio',
          label: 'A',
          value: 'A'
        },
        {
          name: 'type_B',
          type: 'radio',
          label: 'B',
          value: 'B'
        },
        {
          name: 'type_O',
          type: 'radio',
          label: 'O',
          value: 'O'
        },
        {
          name: 'type_AB',
          type: 'radio',
          label: 'AB',
          value: 'AB'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on BloodTypeAlert');
          }
        },
        {
          text: 'Done',
          handler: () => {
            console.log('Confirm done on BloodTypeAlert');
          }
        }
      ]
    });

    await alert.present();
  }

  async rhesusTypeAlert() {
    const alert = await this.alertController.create({
      header: 'What is the type of your blood rhesus?',
      inputs: [
        {
          name: 'type_RHplus',
          type: 'radio',
          label: 'RH+',
          value: 'RH+'
        },
        {
          name: 'type_RHmin',
          type: 'radio',
          label: 'RH-',
          value: 'RH-'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on RhesusTypeAlert');
          }
        },
        {
          text: 'Done',
          handler: () => {
            console.log('Confirm done on RhesusTypeAlert');
          }
        }
      ]
    });

    await alert.present();
  }
  
  async logoutConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Are you sure to logout?',
      buttons:
      [
        {
          text: 'No',
          handler: () => {
            console.log("Confirm cancel on logoutConfirmation");
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log("Confirm done on logoutConfirmation");
          }
        }
      ]
    });

    await alert.present();
  }

}
