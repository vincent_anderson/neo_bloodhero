import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.page.html',
  styleUrls: ['./changepassword.page.scss'],
})
export class ChangepasswordPage implements OnInit {

  constructor(private alertController: AlertController) { }

  ngOnInit() {
  }

  async changeConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Your password has been changed successfully',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on changeConfirmation');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Confirm done on changeConfirmation');
          }
        }
      ]
    });

    await alert.present();
  }

  async deleteAccountConfirmation()
  {
    const alert = await this.alertController.create({
      header: 'Are you sure to delete your account permanently?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler:  () => {
            console.log('Confirm cancel on deleteAccountConfirmation');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Confirm done on deleteAccountConfirmation');
          }
        }
      ]
    });

    await alert.present();
  }

}
