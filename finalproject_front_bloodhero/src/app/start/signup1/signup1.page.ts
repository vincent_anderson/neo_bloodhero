import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Signup1Service } from './signup1.service';
import { AlertController, ActionSheetController } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';

@Component({
  selector: 'app-signup1',
  templateUrl: './signup1.page.html',
  styleUrls: ['./signup1.page.scss'],
})
export class Signup1Page implements OnInit {

  form: FormGroup;
  isLoading = false;
  imagePickerOptions = 
  {
    maximumImagesCount: 1,
    quality: 50
  }

  status_err: number;
  message: string;
  longitude: number = 10.3;
  latitude: number = 11.2;
  profilePicture: string = "tess";
  kode : number;
  tempImage: SafeResourceUrl;
  profPic : any;

  constructor(
    private signUpSvc: Signup1Service, 
    private router: Router, 
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private file: File,
    private camera: Camera,
    private actionSheetCtrl : ActionSheetController,
    private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      fullName: 
        [null,
        Validators.compose(
          [Validators.required,
            Validators.pattern('[a-zA-Z ]*'),
            Validators.maxLength(30)]
        )],
      address:
        [null,
        Validators.required],
      email:
        [null,
        Validators.compose(
          [Validators.required,
          Validators.email]
        )],
      phoneNumber:
        [null,
        Validators.compose(
          [Validators.required,
            Validators.pattern('[0-9]*'),
            Validators.minLength(5)]
        )],
      password:
        [null,
        Validators.compose(
          [Validators.required,
          Validators.minLength(6)]
        )],
      rePassword:
        [null, 
        Validators.compose(
          [Validators.required,
          Validators.minLength(6)]
        )],
      bloodType:
        [null, 
        Validators.required],
      rhesus:
        [null,
        Validators.required]
    })

    console.log(this.form);
  }

  onSignUp() 
  {
    if(this.form.value.password === this.form.value.rePassword) 
    {
      this.signUpSvc.signUp(this.form.value, this.latitude, this.longitude, this.profilePicture).subscribe((data: any) => {
        this.kode= data.code;
        console.log("Ini kode => ", this.kode)
        if(this.kode != 201){
          console.log("EMAIL ATAU NOMOR HAPE SUDAH TERDAFTAR");
          this.repeatedError();
          return;
        } else {
          console.log("hasil form ", this.form.value);
          this.form.reset();
          console.log("FORM KOSONG ", this.form.value);
          this.router.navigateByUrl('/login');
        }
        
      });     
    } 
    else 
    { 
      this.status_err = 1;
      return ;
    }
  }

  async repeatedError()
  {
    const error_alert = await this.alertCtrl.create({
      header: 'Error',
      message: 'This Email and/or Phone Number has been registered before.',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Confirm close on repeatedError');
          }
        }
      ]
    });

    await error_alert.present();
  }

  // pickImage(sourceType)
  // {
  //   const options: CameraOptions = 
  //   {
  //     quality: 100,
  //     sourceType: sourceType,
  //     destinationType: this.camera.DestinationType.FILE_URI,
  //     encodingType: this.camera.EncodingType.JPEG,
  //     mediaType: this.camera.MediaType.PICTURE
  //   }
  //   this.camera.getPicture(options).then((imageData) => {
  //     //if base64 (BASE_URL);
  //     let base64image = 'data:image/jpeg;base64,' + imageData;
  //     // send image to endpoint /upload here.
  //   }, (err) => {
  //     console.log('Error on pickImage()');
  //   });
  // }

  async showMe(data: any)
  {
     const alert = await this.alertCtrl.create({
       message: data,
       buttons: [{
         text: 'close',
         role: 'cancel'
       }]
     })
  }

  async pickPhoto()
  {
    const { Camera } = Plugins;
    const result = await Camera.getPhoto({
      quality: 100,
      allowEditing: false,
      source: CameraSource.Photos,
      resultType: CameraResultType.DataUrl,
    });

    this.tempImage = this.sanitizer.bypassSecurityTrustResourceUrl(
      result && (result.dataUrl),
    );

    this.profPic = this.tempImage;
    // this.showMe(this.profPic);
  }

  async takePhoto()
  {
    const { Camera } = Plugins;
    const result = await Camera.getPhoto({
      quality: 100,
      allowEditing: false,
      source: CameraSource.Camera,
      resultType: CameraResultType.DataUrl,
    });

    this.tempImage = this.sanitizer.bypassSecurityTrustResourceUrl(
      result && (result.dataUrl),
    );

    this.profPic = this.tempImage;
  }

  // async showMe(data: any)
  // {
  //   const alert = await this.actionSheetCtrl.create({
  //     header: data,
  //     buttons: [{
  //       text: 'Cancel',
  //       role: 'cancel'
  //     }]
  //   });

  //   await alert.present();
  // }

  async selectImage()
  {
    const actionSheetCtrl = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
            console.log("Enter selectImage() on Load from Library");
            this.pickPhoto();
            // this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePhoto();
            // this.pickImage(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });

    await actionSheetCtrl.present();
  }
}
