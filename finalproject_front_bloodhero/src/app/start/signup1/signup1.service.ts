import { userSignUp } from './../../model/model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
// tslint:disable-next-line: import-blacklist
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable({
  providedIn: 'root'
})
export class Signup1Service {

  // ENDPOINT
  url = 'https://backtoneobloodhero.herokuapp.com/signup';
  url_upload_picture = 'https://backtoneobloodhero.heroku.com/upload';

  constructor(private http: HttpClient) { }

  // signUp(signUpData: userSignUp, latitude: number, longitude: number, profilePicture: string): Observable<Response> {
  //   const reqHeader = new Headers({});
  //   // reqHeader.append('Content-Type', 'application/json');
  //   return this.http.post(this.url, {signUpData, latitude, longitude, profilePicture}, {headers: reqHeader})
  // }
  hasil: any;

  upload(profilePicture: File) {
    const reqHeader = new HttpHeaders({});

    return this.http.post(this.url_upload_picture, profilePicture, {headers: reqHeader});
  }

  signUp(signUpData: userSignUp, latitude: number, longitude: number, profilePicture: string) {
    const reqHeader = new HttpHeaders({});

    return this.http.post(this.url, {signUpData, latitude, longitude, profilePicture}, {headers: reqHeader});
    // .subscribe((data: Response) => {
    //   console.log("TESSSSSSSSSSSSS",data);
    //   this.hasil = data;
    //   console.log("data HASIL ",this.hasil.code);
    //   return this.hasil;
    //   // if(this.hasil.code !== 201)
    // }, (error:any) => {
    //   console.log(error);
    // });

    // .map((res: Response) => {
    //    if (res) {
    //      console.log("res => ", res)

        //  if (res.status === 201 ) {
        //    console.log("akun berhasil didaftarkan")
        //   return [{status: res, json: res}]
        //  }
    //    }
    //   })
    // .catch((error: any) => {
    //   if (error.status === 403) {
    //     console.log("email/nomor hape sudah terdaftar")
    //     return Observable.throw(new Error(error.status));
    //   }
    // });
  }
}