import { userLogin } from './../../model/model';
import { LoginService } from './login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  userInput: userLogin;
  isLoading = false;
  form: FormGroup;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController) { }

  ngOnInit() {
    // this.loginService.login();
    this.form = this.formBuilder.group({
      email:
      [null,
      Validators.compose(
        [Validators.required,
        Validators.email]
      )],
      password:
      [null,
      Validators.compose(
        [Validators.required,
        Validators.minLength(6)]
      )]
    })
  }

  onLogin() {
    console.log("TOMBOL LOGIN DITEKAN")
    this.userInput = this.form.value;
    console.log("data front-end => ", this.userInput);
    this.isLoading = true;
    this.loadingCtrl.create({
      keyboardClose: true, message:
      'Logging in...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.isLoading = false;
          loadingEl.dismiss();

          this.loginService.login(this.userInput).subscribe((data: any) => {
            console.log("data login ", data);
            if(data.code === 500 ){ 
              console.log("Server Down !");
              this.serverError();
              this.form.reset();
            } else if(data.code === 404) {
              console.log("User tidak terdaftar !");
              this.unregisteredError();
              this.form.reset();
            } else if( data.code === 401 ) { 
              console.log("Password salah !");
              this.inputError();
              this.form.reset();
            } else if(data.code === 200) {
              console.log("User berhasil login !");
              this.form.reset();
              environment.jwt_token = data.access_token;
              console.log("token taken by environment.jwt_token: ", environment.jwt_token);
              this.router.navigateByUrl('/home/tabs/nearbypeople');
            }

          },
          (err: HttpErrorResponse) => {
            console.log(err)
            });
          // console.log(this.userInput);

        }, 1500);
      });
    // this.loginService.login();
    console.log("PROSES LOGIN SELESAI")
  }

  //server down
  async serverError()
  {
    const error_alert = await this.alertCtrl.create({
      header: 'Error',
      message: 'Connection to server can\'t be established for now. Check your internet connection',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Confirm close on serverError');
          }
        }
      ]
    });

    await error_alert.present();
  }

  //unregistered
  async unregisteredError()
  {
    const error_alert = await this.alertCtrl.create({
      header: 'Error',
      message: 'Your account can\'t be found. Have you registered yet?',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Confirm close on unregisteredError');
          }
        },
        {
          text: 'Register',
          handler: () => {
            console.log('Confirm register on unregisteredError');
            this.router.navigateByUrl('/signin');
          }
        }
      ]
    });

    await error_alert.present();
  }

  async inputError()
  {
    const error_alert = await this.alertCtrl.create({
      header: 'Error',
      message: 'Your email and/or password is wrong. Input again.',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Confirm close on inputError');
          }
        }
      ]
    });

    await error_alert.present();
  }

}
