import { userLogin } from './../../model/model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url = 'https://backtoneobloodhero.herokuapp.com/';
  // url = 'http://localhost:3000/';

  private _userIsAuthenticated = false;

  get userIsAuthenticated() {
    return this._userIsAuthenticated;
  }

  constructor(private http: HttpClient) { }

  login(user: userLogin) {
    console.log("login data ", user);
    // const reqHeader = new http ==> HTTPHeaders ga bisa di import disini
    const reqHeader = new HttpHeaders({});
    this._userIsAuthenticated = true;
    return this.http.post(this.url, user, {headers: reqHeader});
    // this._userIsAuthenticated = true;
  }

  logout() {
    this._userIsAuthenticated = false;
  }

}
