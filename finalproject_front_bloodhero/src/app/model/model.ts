export interface userLogin {
    email: string,
    password: string
}

export interface userSignUp {
    name: string,
    email: string,
    address: string,
    password: string,
    phone_number: string,
    blood_type: string,
    rhesus: string,
    profile_picture: string
}

export interface updateProfileData {
    name: string,
    address: string,
    phone_number: string,
    profile_picture: string
}

export interface showUserProfile {
    name: string,
    address: string,
    phone_number: string,
    profile_picture: string,
    blood_type: string,
    rhesus: string
}

export interface changePassword {
    oldPassword: string,
    password: string,
    rePassword: string
}

export interface locationCoords {
    latitude?: number,
    longitude?: number
}

export interface history{
    donor_date: Date,
    description: string
}
export interface NearbyUsers {
    user_id: number,
    name: string,
    email: string,
    address: string,
    password: string,
    phone_number: string,
    blood_type: string,
    rhesus: string,
    profile_picture: string,
    longitude: number,
    latitude: number
}

export interface Data {
    userData: NearbyUsers;
    distance: number;
}

export interface ListUser {
    message: string;
    code: number;
    data: Data[];
}

export interface DetailUser {
    name: string;
    profile_picture: string;
    address: string;
    email: string;
    phone_number: string;
    blood_type: string;
    rhesus: string;
}

export interface Notifications {
    user_id: number;
    name: string;
    distance: number;
}
