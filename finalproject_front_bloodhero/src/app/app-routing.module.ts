// import { TabsPage } from './tabs/tabs.page';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginGuard } from './start/login/login.guard';

const routes: Routes = [

  {
    path: 'login',
    loadChildren: './start/login/login.module#LoginPageModule'
  },
  {
    path: 'home',
    loadChildren: './tabs/tabs.module#TabsPageModule',
    canLoad: [LoginGuard]
  },
  {
    path: '',
    redirectTo : 'home',
    pathMatch: 'full'
  },
  // === versi awal ===

  // {
  //   path: '',
  //   redirectTo: 'login',
  //   pathMatch: 'full'
  // },
  // {
  //   path: 'login',
  //   loadChildren: './start/login/login.module#LoginPageModule'
  // },
  {
    path: 'signin',
    children: [
      {
        path: '',
        loadChildren: './start/signup1/signup1.module#Signup1PageModule'
      },
      {
        path: 'personal',
        loadChildren: './start/signup1/signup1.module#Signup1PageModule' 
      },
      {
        path: 'blood',
        loadChildren: './start/signup2/signup2.module#Signup2PageModule'
      }
    ]
  },
  { path: 'about', loadChildren: './start/about/about.module#AboutPageModule' },
  // { path: 'auth', loadChildren: './start/login/login.module#LoginPageModule' },

  // {
  //   path: 'nearbypeople',
  //   children: [
  //     {
  //       path: '',
  //       loadChildren: './nearbypeople/search/search.module#SearchPageModule'
  //     },
  //     {
  //       path: 'list',
  //       loadChildren: './nearbypeople/list/list.module#ListPageModule'
  //     },
  //     {
  //       path: 'found',
  //       loadChildren: './nearbypeople/foundprofile/foundprofile.module#FoundprofilePageModule'
  //     },
  //     {
  //       path: 'donate',
  //       loadChildren: './nearbypeople/needdonation/needdonation.module#NeeddonationPageModule'
  //     },
  //     {
  //       path: 'recipient',
  //       loadChildren: './nearbypeople/foundneedprofile/foundneedprofile.module#FoundneedprofilePageModule'
  //     }
  //   ]
  // },
  // {
  //   path: 'donorhistory',
  //   children: [
  //     {
  //       path: '',
  //       loadChildren: './donorhistory/history/history.module#HistoryPageModule'
  //     },
  //     {
  //       path: 'history',
  //       loadChildren: './donorhistory/history/history.module#HistoryPageModule'
  //     },
  //     {
  //       path: 'add',
  //       loadChildren: './donorhistory/add/add.module#AddPageModule'
  //     }
  //   ]
  // },
  // {
  //   path: 'profile',
  //   children: [
  //     {
  //       path: '',
  //       loadChildren: './profile/profile/profile.module#ProfilePageModule'
  //     },
  //     {
  //       path: 'profile',
  //       loadChildren: './profile/profile/profile.module#ProfilePageModule'
  //     },
  //     {
  //       path: 'change',
  //       loadChildren: './profile/changepassword/changepassword.module#ChangepasswordPageModule'
  //     }
  //   ]
  // }
  // {
  //   path: '',
  //   loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  // },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
