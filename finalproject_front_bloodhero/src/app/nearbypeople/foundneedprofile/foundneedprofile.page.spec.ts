import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoundneedprofilePage } from './foundneedprofile.page';

describe('FoundneedprofilePage', () => {
  let component: FoundneedprofilePage;
  let fixture: ComponentFixture<FoundneedprofilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoundneedprofilePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundneedprofilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
