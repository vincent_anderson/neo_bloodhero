// import { DataService } from './../data.service';
import { locationCoords } from './../../model/model';
// import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Geolocation } from '@capacitor/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  bloodForm: FormGroup;

  coords: locationCoords;

  constructor(private alertController : AlertController) {
    // this.getLocation();
   }

  

  ngOnInit() {
    this.bloodForm = new FormGroup({
      profile: new FormControl('same', {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      type: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      })
    });

    console.log(this.bloodForm);
  }

    // requestGPSPermission()
  // {
  //   this.locationAccuracy.canRequest().then((canRequest: boolean) => {
  //     if(canRequest)
  //     {
  //       console.log("4");
  //     }
  //     else
  //     {
  //       //show 'GPS Permission Request' dialogue
  //       this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
  //       .then(
  //         () => {
  //           this.askToTurnOnGPS();
  //         },
  //         error => {
  //           alert('requestPermission Error requestion location permission')
  //         }
  //       );
  //     }
  //   });
  // }


  //  askToTurnOnGPS()
  // {
  //   this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
  //     () => {
  //       this.getLocation();

  //     },
  //     error => alert('Error requesting location permissions')
  //   );
  // }

  // async typeFirstChoice() {
  //   const alert = await this.alertController.create({
  //     header: 'What type of blood you are looking for?',
  //     inputs: [
  //       {
  //         name: 'same',
  //         type: 'radio',
  //         label: 'The same with my profile',
  //         value: 'same'
  //       },
  //       {
  //         name: 'different',
  //         type: 'radio',
  //         label: 'Different from my profile',
  //         value: 'different'
  //       }
  //     ],
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         role: 'cancel',
  //         handler: () => {
  //           console.log('Confirm cancel on typeFirstChoice');
  //         }
  //       },
  //       {
  //         text: 'Done',
  //         handler: () => {
  //           console.log('Confirm done on typeFirstChoice');
  //         }
  //       }
  //     ]
  //   });

  //   await alert.present();
  // }

  // async typeSecondChoice() {
  //   const alert = await this.alertController.create({
  //     header: 'What blood type you are looking for?',
  //     inputs: [
  //       {
  //         name: 'type_A',
  //         type: 'radio',
  //         label: 'A',
  //         value: 'A'
  //       },
  //       {
  //         name: 'type_B',
  //         type: 'radio',
  //         label: 'B',
  //         value: 'B'
  //       },
  //       {
  //         name: 'type_O',
  //         type: 'radio',
  //         label: 'O',
  //         value: 'O'
  //       },
  //       {
  //         name: 'type_AB',
  //         type: 'radio',
  //         label: 'AB',
  //         value: 'AB'
  //       }
  //     ],
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         role: 'cancel',
  //         handler: () => {
  //           console.log('Confirm cancel on BloodTypeAlert');
  //         }
  //       },
  //       {
  //         text: 'Done',
  //         handler: () => {
  //           console.log('Confirm done on BloodTypeAlert');
  //         }
  //       }
  //     ]
  //   });

  //   await alert.present();
  // }

}
