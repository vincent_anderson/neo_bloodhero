import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  constructor(private alertController: AlertController) { }

  ngOnInit() {
  }

  async endConfirmationAlert() {
    const alert = await this.alertController.create({
      header: '<strong>If you continue, this list will be erased and you will be returned to Search Page.<br>Are you sure to continue?</strong>',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Confirm cancel on endConfirmationAlert');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Confirm yes on endConfirmationAlert');
          }
        }
      ]
    });

    await alert.present();
  }
  

}
