# NEO_BloodHero

Ini project mobile Nixerson paling baru karena yang lama tragedi
---

**Jangan lupa:**
npm install

Untuk **test endpoint** upload, buat folder yang namanya `upload` di dalam folder finalproject_front_bloodhero

---
Task List : *waiting*
- [ ] Backend untuk Search, Donor History
- [ ] Upload file di frontend.
- [ ] Backend untuk Forget Password, Update Profile
- [ ] Backend untuk Register (data gambar + lokasi)
- [ ] Infinite Scrolling (?) untuk data profile yang ditemukan, donor history

- [ ] Testing di device untuk tes Lokasi & Telepon

Task List : *done*
- [X] Backend untuk Login, Register (untuk data non-gambar)

